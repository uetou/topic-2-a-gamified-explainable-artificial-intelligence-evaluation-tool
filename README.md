# **Topic 2 – A Gamified Explainable Artificial Intelligence Evaluation Tool**

This project originates from a university course **"Developing Socio-technical Information Systems"**  where we were tasked with developing a gamified web-app to help experts evaluate XAI. This project allowed us to apply our knowledge of web development and game design principles to create an engaging and educational tool. Medical images from cancer scans, heatmaps and survival predictions ([🔗](http://pancancer.mahmoodlab.org)) are the main contents of our example use case.

Our approach and game design choices were heavily influenced by the research presented in the paper: **"Designing Gamification Concepts for Expert Explainable Artificial Intelligence Evaluation Tasks: A Problem Space Exploration"**

To create an engaging and nostalgic experience, we drew inspiration from the vibrant visuals and playful sounds of classic 80s arcade games. Can you uncover all 6 out of 7 implementations of expert requirements within the design?

# Project Setup Guide

This guide outlines the requirements and steps needed to set up and run this web application project, which utilizes the [MERN](https://www.mongodb.com/mern-stack) stack.

## Prerequisites

Before you start, ensure you have the following installed:

- **Git**: Used for version control. [Download Git](https://git-scm.com/downloads)

- **Node.js and npm**: Node.js is the runtime environment required to run the server, and npm is the package manager for managing dependencies. [Download Node.js and npm](https://nodejs.org/en/download/)

- **MongoDB Community Edition**: This is the database used for the project. Follow the instructions to [install MongoDB Community Edition](https://docs.mongodb.com/manual/installation/).

## Installation

After setting up the prerequisites, follow these steps to install the necessary packages for the project:

1. Clone the repository and navigate to the project directory:
    ```bash
    git clone git@gitlab.kit.edu:uetou/topic-2-a-gamified-explainable-artificial-intelligence-evaluation-tool.git
    ```
    ```bash
    cd topic-2-a-gamified-explainable-artificial-intelligence-evaluation-tool
    ```

1. Install the necessary dependencies:
    ```bash
    npm install
    ``` 

## Running the Application

### Database

If this is the first time building the project, you have to set up the MongoDB database:

1. Open MongoDB Shell:
   ```bash
   mongosh
   ```
2. Switch to the `aid-game-database` database (it will be created if it doesn't exist):
   ```bash
   use aid-game-database
   ```
    Output:
   ```
   switched to db aid-game-database
   ```

3. Show all databases to confirm:
   ```bash
   show dbs
   ```
    Output should be something like this:
   ```
   admin               XX.XX KiB
   aid-game-database   XX.XX KiB
   config              XX.XX KiB
   local               XX.XX KiB
   ```
   

### Backend

1. Navigate to the backend folder:
    ```bash
    cd aid-game-backend
    ```

2. Run the `importData.js` script to load the example data:

    ```bash
    node importData.js
    ```

    Note: if you want to use different data, point the script to you json file, or alternatively modify the contents of the `data.json`.

3. Start the backend server by running:
    ```bash
    node server.js
    ```

### Frontend

1. In another shell tab, navigate to the frontend folder:
    ```bash
    cd aid-game
    ```

2. Start the frontend server:
   ```bash
   npm start
   ```

[//]: # (Removed Code design and added link to the MERN stack)
## **Disclaimer**

TUMOR TAKEDOWN works best on **Safari** & **Google Chrome**

## Development Team

- Aleksandar Bachvarov
- Nhung Tran
- Frithjof Anton Ingemar Eisenlohr 