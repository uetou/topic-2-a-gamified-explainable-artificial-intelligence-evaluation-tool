import React, { useState } from 'react';
import Leaderboard from './Leaderboard.js';
import './LoginPage.css';

const LoginPage = ({ onStartClick }) => {
  const [username, setUsername] = useState('');
  const [link, setLink] = useState('');

  const handleInputChange = (e) => {
    setUsername(e.target.value);
  };

  const handleLink = (e) => {
    setLink(e.target.value);
  }

  const handleStartClick = () => {
    if (username.trim() !== '') {
        fetch('http://localhost:3001/api/expert', {
            method: 'POST',
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({ name: username, link: link })
        })
        .then(response => response.json())
        .then(data => {
            console.log(data);
            onStartClick(username);
        })
        .catch(error => console.error('Error:', error));
    } else {
        alert('Please enter your username.');
    }
};

  return (
    <div className='login-container'>
      <h2 className='textLP'>Welcome!</h2>
      <p className='textLP'>Enter your username:</p> <br></br>
      <div className='text-desc'>Username:</div> <input type='textLP' value={username} onChange={handleInputChange} className='text-box-LP'/>
      <div className='text-desc'>Organization (link):</div> <input type='textLP' value={link} onChange={handleLink} className='text-box-link'/>
      <button onClick={handleStartClick} className='buttonLP'>Start</button>
      <Leaderboard />
    </div>
  );
};

export default LoginPage;
