import './BackgroundMusic.css'

function BackgroundMusic({ play }) {
    return (
        play && <div>
            <iframe
                width="1"
                height="1"
                src="https://www.youtube.com/embed/4xDzrJKXOOY?autoplay=1"
            ></iframe>
        </div>
    );
}

export default BackgroundMusic;