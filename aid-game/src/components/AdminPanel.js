import React, { useState, useEffect } from 'react';
import './AdminPanel.css';

function AdminPanel() {
    const [cases, setCases] = useState([]);
    const [experts, setExperts] = useState([]);
    const [expandedCases, setExpandedCases] = useState(new Set());
    const [expandedExperts, setExpandedExperts] = useState(new Set());

    useEffect(() => {
        fetch('http://localhost:3001/api/case')
            .then(response => response.json())
            .then(data => setCases(data))
            .catch(error => console.error('Error fetching cases:', error));

        fetch('http://localhost:3001/api/expert')
            .then(response => response.json())
            .then(data => setExperts(data))
            .catch(error => console.error('Error fetching experts:', error));
    }, []);

    const toggleCaseDetails = (caseId) => {
        const newExpandedCases = new Set(expandedCases);
        if (newExpandedCases.has(caseId)) {
            newExpandedCases.delete(caseId);
        } else {
            newExpandedCases.add(caseId);
        }
        setExpandedCases(newExpandedCases);
    };

    const toggleExpertDetails = (expertName) => {
        const newExpandedExperts = new Set(expandedExperts);
        if (newExpandedExperts.has(expertName)) {
            newExpandedExperts.delete(expertName);
        } else {
            newExpandedExperts.add(expertName);
        }
        setExpandedExperts(newExpandedExperts);
    };

    const calculateAverage = (arr) => {
        if (!arr || arr.length === 0) {
            return "No data";
        }
        const sum = arr.reduce((a, b) => a + b, 0);
        return (sum / arr.length).toFixed(2); 
    };

    return (
        <div className='admin'>
            <h1 className='title'>Admin Panel</h1>
            <div style={{ display: 'flex', justifyContent: 'space-around', alignItems: 'flex-start'}}>
                <div style={{ flex: 1, margin: '0 10px' }}> 
                    <h2>Cases</h2>
                    <table>
                    <thead>
                        <tr>
                            <th>Case ID</th>
                            <th>Source Image</th>
                            <th>Heatmap Image</th>

                        </tr>
                    </thead>
                    <tbody>
                        {cases.map((caseItem, index) => (
                            <>
                                <tr key={index} onClick={() => toggleCaseDetails(caseItem.case_id)}>
                                    <td>{caseItem.case_id}</td>
                                    <td>
                                        <img style={{ width:70, height:70 }} src={caseItem.source_image} />
                                    </td>
                                    <td>
                                        <img style={{ width:70, height:70 }} src={caseItem.heatmap_image} />
                                    </td>

                                </tr>
                                {expandedCases.has(caseItem.case_id) && (
                                    <tr>
                                        <td colSpan="11">
                                            <p>Explanation: {caseItem.explanation_text}</p>
                                            <p>Average Accuracy: {calculateAverage(caseItem.accuracy)}, Values: {caseItem.accuracy.join("; ")}</p>
                                            <p>Average Clarity: {calculateAverage(caseItem.clarity)}, Values: {caseItem.clarity.join("; ")}</p>
                                            <p>Average Consistency: {calculateAverage(caseItem.consistency)}, Values: {caseItem.consistency.join("; ")}</p>
                                            <p>Average Usefulness: {calculateAverage(caseItem.usefulness)}, Values: {caseItem.usefulness.join("; ")}</p>
                                            <p>Average Confidence: {calculateAverage(caseItem.confidence)}, Values: {caseItem.confidence.join("; ")}</p>
                                            <p>Average Reliability: {calculateAverage(caseItem.reliability)}, Values: {caseItem.reliability.join("; ")}</p>
                                            <p>Additional Remarks: {caseItem.additionalRemarks.join("; ")}</p>
                                        </td>
                                    </tr>
                                )}
                            </>
                        ))}
                    </tbody>
                </table>
                </div>
                <div style={{ flex: 1, margin: '0 10px' }}> 
                    <h2>Experts</h2>
                    <table>
                    <thead>
                        <tr>
                            <th>Name</th>
                        </tr>
                    </thead>
                    <tbody>
                        {experts.map((expert, index) => (
                            <>
                                <tr key={index} onClick={() => toggleExpertDetails(expert.name)}>
                                    <td>{expert.name}</td>
                                </tr>
                                {expandedExperts.has(expert.name) && (
                                    <tr>
                                        <td colSpan="4"> 
                                            <p>Points: {expert.points}</p>
                                            <p>Level: {expert.level}</p>
                                            <p>Last Point Update: {new Date(expert.lastPointUpdate).toLocaleString()}</p>
                                        </td>
                                    </tr>
                                )}
                            </>
                        ))}
                    </tbody>
                </table>
                </div>
            </div>
        </div>
    );
}

export default AdminPanel;
