import React, { useState } from 'react';
import './FeedbackForm.css';

function FeedbackForm({ imageIdentity, onSubmitFeedback, username }) {
    const [detail, setDetail] = useState('');
    const [accuracy, setAccuracy] = useState(0);
    const [clarity, setClarity] = useState(0);
    const [consistency, setConsistency] = useState(0);
    const [usefulness, setUsefulness] = useState(0);
    const [confidence, setConfidence] = useState(0);
    const [reliability, setReliability] = useState(0);
    const [ratingChanged, setRatingChanged] = useState(false);

    const handleDetailChange = (e) => {
        setDetail(e.target.value);
        setRatingChanged(true);
    }
    const handleAccuracyChange = (e) => {
        setAccuracy(e.target.value);
        setRatingChanged(true);
    }
    const handleClarityChange = (e) => {
        setClarity(e.target.value);
        setRatingChanged(true);
    }
    const handleConsistencyChange = (e) => {
        setConsistency(e.target.value);
        setRatingChanged(true);
    }
    const handleUsefulnessChange = (e) => {
        setUsefulness(e.target.value);
        setRatingChanged(true);
    }
    const handleConfidenceChange = (e) => {
        setConfidence(e.target.value);
        setRatingChanged(true);
    }
    const handleReliabilityChange = (e) => {
        setReliability(e.target.value);
        setRatingChanged(true);
    }

    const handleSubmit = (e) => {
        e.preventDefault();
        const feedbackData = {
            caseId: imageIdentity,
            accuracy,
            clarity,
            consistency,
            usefulness,
            confidence,
            reliability,
            userRatings: {
                accuracy,
                clarity,
                consistency,
                usefulness,
                confidence,
                reliability,
            },
            additionalRemarks: detail,
            username
        };

        fetch('http://localhost:3001/api/feedback', {
            method: 'POST',
            headers: { 'Content-Type': 'application/json' },
            body: JSON.stringify(feedbackData)
        })
            .then(response => response.json())
            .then(data => {
                console.log(data);
                onSubmitFeedback(imageIdentity, feedbackData.userRatings);
            })
            .catch(error => console.error('Error:', error));

        setAccuracy(0);
        setClarity(0);
        setConsistency(0);
        setUsefulness(0);
        setConfidence(0);
        setReliability(0);
        setDetail('');
    };

    const renderSlider = (label, value, change) => {
        return (
            <label>
                {label}: {value} /10<br></br>
                <input
                    type="range"
                    min="1"
                    max="10"
                    value={value}
                    onChange={change}
                    className='slider'
                    style={{
                        background: `linear-gradient(
                            to right, 
                            transparent ${value * 10}%, 
                            #cccccc ${value * 10}%,
                            transparent ${value * 10 + 10}%, 
                            #cccccc ${value * 10}%,
                            transparent ${value * 10 + 20}%,
                            #cccccc ${value * 10}%,
                            transparent ${value * 10 + 30}%,
                            #cccccc ${value * 10}%,
                            transparent ${value * 10 + 40}%,
                            #cccccc ${value * 10}%,
                            transparent ${value * 10 + 50}%,
                            #cccccc ${value * 10}%,
                            transparent ${value * 10 + 60}%,
                            #cccccc ${value * 10}%,
                            transparent ${value * 10 + 70}%,
                            #cccccc ${value * 10}%,
                            transparent ${value * 10 + 80}%,
                            #cccccc ${value * 10}%,
                            transparent ${value * 10 + 90}%,
                            #cccccc ${value * 10}%,
                            transparent ${value * 10 + 100}%,
                            #cccccc ${value * 10}%`
                    }} />
                <br></br>
                <br></br>
            </label>
        );
    };

    return (
        <form onSubmit={handleSubmit}>
            <div>
                {renderSlider("Accuracy of Localization:", accuracy, handleAccuracyChange)}
                {renderSlider("Clarity of Heatmap:", clarity, handleClarityChange)}
                {renderSlider("Consistency with Medical Knowledge:", consistency, handleConsistencyChange)}
                {renderSlider("Usefulness for Diagnosis:", usefulness, handleUsefulnessChange)}
                {renderSlider("Confidence Level:", confidence, handleConfidenceChange)}
                {renderSlider("Overall Reliability:", reliability, handleReliabilityChange)}
                <label>
                    Additional remarks: <br></br>
                    <textarea className='textarea' value={detail} onChange={handleDetailChange} />
                </label>
            </div>
            <button className='submit-button' disabled={!ratingChanged}>Submit Feedback</button>
        </form>
    );
}

export default FeedbackForm;
