import React from 'react';
import './Modal.css';

function Modal({ show, onClose, children }) {
    if (!show) {
        return null;
    }
    const handleClose = () => {
        onClose(); 
    };
    return (
        <div className="modal" onClick={onClose}>
            <button className="modal-close-button" onClose={handleClose}>
                &times; 
            </button>
            <div className="modal-content" onClick={e => e.stopPropagation()}>
                {children}
            </div>
        </div>
    );
}

export default Modal;
