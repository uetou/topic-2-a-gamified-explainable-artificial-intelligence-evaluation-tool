import React, { useState, useEffect } from 'react';
import './QuizGame.css'

function QuizGame({ questions, onFinish }) {
    const [selectedQuestion, setSelectedQuestion] = useState(null);
    const [selectedAnswer, setSelectedAnswer] = useState('');
    const [showAnswer, setShowAnswer] = useState(false);
    const [feedbackMessage, setFeedbackMessage] = useState('');

    useEffect(() => {
        const randomIndex = Math.floor(Math.random() * questions.length);
        setSelectedQuestion(questions[randomIndex]);
    }, [questions]);


    const handleAnswerSelect = (answer) => {

        setSelectedAnswer(answer);
        setShowAnswer(true);

        if (answer === selectedQuestion.answer) {
            setFeedbackMessage('Correct!');
            onFinish(5);
        } else {
            setFeedbackMessage('Better luck next time!');
            onFinish(0);
        }
    };

    if (!selectedQuestion) return <div>Loading...</div>;

    return (
        <div>
            <div>
                <h3 className='textQ-head'>Time for a short quiz!</h3>
                <p className='textQ-title'>{selectedQuestion.question}</p>
                <div></div>
                {selectedQuestion.options.map(option => (
                    <button
                        className='buttonQ'
                        key={option}
                        onClick={() => handleAnswerSelect(option)}
                        disabled={showAnswer}
                        style={{
                            margin: '5px',
                            borderColor: showAnswer ? (option === selectedQuestion.answer ? '#7fbc00' : (option === selectedAnswer ? '#fd0000' : '')) : ''
                        }}
                    >
                        {option}
                    </button>
                ))}
                <div>
                    {showAnswer && <p className='feedback'>{feedbackMessage}</p>}
                </div>
            </div>
        </div>
    );
}

export default QuizGame;
