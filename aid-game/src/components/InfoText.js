import './InfoText.css'

function InfoText() {
    return (
        <div>
            <p className='text'>Information</p>
            <br></br>
            <p className='header'> Welcome to TUMOR TAKEDOWN!</p>
            <br></br>
            <p className='text2'> I am a new game to help my sister, AI, to find specific markers of a tumor. </p>
            <p className='text2'> The AI has already generated markers of tumor or a description of potential tumor markers.</p>
            <p className='text2'> My task is to get feedback for my AI sister, so she can improve her skills.</p>
            <p className='text2'> This is a very important topic and allows tumors to be found more quickly and saves people’s lives!</p>
            <p className='text2'> To help my sister, patients and me, I need YOU!</p>
            <p className='text2'> You want to WIN, be on THE ALL-TIME LEADERBOARD and save THOUSANDS OF LIVES? Then earn your points!</p>
            <br></br>
            <p className='text2'> How to get points: </p>
            <p className='text2'> 1. Rating Pictures: 5 Points per Slider and 10 Points Comment Box</p>
            <p className='text2'> 2. After some levels, there will be additional games:</p>
            <p className='text3'>   • Quizgame: 5 Points</p>
            <p className='text3'>   • Memorygame: 15 Points</p>
            <p className='text4'>
                Have fun and thank you very much for your help! <br></br><br></br>
                Aleksandar, Frithjof and Nhung
            </p>
            <p className='text2'> Do you need more information and help ? </p>
            <p className='text2'> On <a href='http://pancancer.mahmoodlab.org/' target="_blank">this page</a> you can find more information about the tumor, which can help to improve your score.</p>
        </div>
    );
}

export default InfoText;