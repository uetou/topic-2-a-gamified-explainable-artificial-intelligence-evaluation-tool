import React from 'react';

function CaseDisplay({ case_id }) {
    return (
        <div>
            <img src={case_id.source_image} alt={case_id.heatmap_image} />
            <img src={case_id.heatmap_image} alt={case_id.heatmap_image} />
            <p>{case_id.description}</p>
        </div>
    );
}

export default CaseDisplay;
