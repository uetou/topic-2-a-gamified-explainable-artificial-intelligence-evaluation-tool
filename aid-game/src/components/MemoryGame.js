import React, { useState, useEffect } from 'react';
import './MemoryGame.css';

function MemoryGame({ cards, onFinish }) {
    const [gameCards, setGameCards] = useState([]);
    const [flippedCards, setFlippedCards] = useState([]);
    const [matchedPairs, setMatchedPairs] = useState([]);
    const [secondsElapsed, setSecondsElapsed] = useState(0);
    const [gameOver, setGameOver] = useState(false);

    useEffect(() => {
        initializeGame();
    }, [cards]);

    useEffect(() => {
        let timer;
        if (!gameOver) {
            timer = setInterval(() => {
                setSecondsElapsed(prevSeconds => prevSeconds + 1);
            }, 1000);
        }

        return () => clearInterval(timer);
    }, [gameOver]);

    const initializeGame = () => {
        let initializedCards = [...cards, ...cards]
            .map(card => ({ ...card, id: Math.random(), flipped: false, matched: false }))
            .sort(() => Math.random() - 0.5);

        setGameCards(initializedCards);
        setFlippedCards([]);
        setMatchedPairs([]);
    };

    const handleCardClick = (index) => {
        if (flippedCards.length < 2 && !gameCards[index].flipped) {
            let newFlippedCards = [...flippedCards, index];
            setFlippedCards(newFlippedCards);
            let newGameCards = [...gameCards];
            newGameCards[index].flipped = true;
            setGameCards(newGameCards);

            console.log(gameCards[index].url);

            if (newFlippedCards.length === 2) {

                const firstCard = gameCards[newFlippedCards[0]];
                const secondCard = gameCards[newFlippedCards[1]];

                if (firstCard.url === secondCard.url) {
                    setMatchedPairs([...matchedPairs, firstCard.url])
                    setFlippedCards([]);

                    newGameCards[newFlippedCards[0]].matched = true;
                    newGameCards[newFlippedCards[1]].matched = true;
                    setGameCards(newGameCards);

                    if (matchedPairs.length + 1 === cards.length) {
                        setGameOver(true);
                        const clearCards = gameCards.map(card => ({ ...card, matched: false }));
                        setGameCards(clearCards);
                        onFinish(15);
                    }
                } else {
                    setTimeout(() => {
                        newGameCards[newFlippedCards[0]].flipped = false;
                        newGameCards[newFlippedCards[1]].flipped = false;
                        setGameCards(newGameCards);
                        setFlippedCards([]);
                    }, 1000);
                }
            }
        }
    };

    const totalPairs = cards.length;
    const pairsMatched = matchedPairs.length;
    const progress = (pairsMatched / totalPairs) * 100;

    return (
        <div className="memory-game">
            {gameCards.map((card, index) => (
                <div
                    key={card.id}
                    className={`memory-card ${card.flipped ? 'flipped' : ''} ${card.matched ? 'matched' : ''}`}
                    onClick={() => handleCardClick(index)}>
                    <div className={`card-inner ${card.flipped ? 'flipped' : ''}`}>
                        <div className="front" style={{ backgroundImage: `url(${card.url})` }}></div>
                        <div className="back"></div>
                    </div>
                </div>
            ))}
            <div className="progress-bar-container">
                <div className="progress-bar" style={{ width: `${progress}%` }}></div>
            </div>
            {gameOver ? (
                <div className="game-over-message">Done in {secondsElapsed}s!
                </div>
            ) : (<div className="timer">Time: {secondsElapsed}</div>)}
            <p className='text-right'>Play and earn</p>
            <p className='text-left'>15 points!</p>
        </div>
    );
}


export default MemoryGame;
