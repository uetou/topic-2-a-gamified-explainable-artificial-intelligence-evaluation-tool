import React, { useState, useEffect } from 'react';
import './Leaderboard.css'

function Leaderboard() {
    const [allTimeExperts, setAllTimeExperts] = useState([]);
    const [todayExperts, setTodayExperts] = useState([]);

    useEffect(() => {
        fetch('http://localhost:3001/api/expert/top/alltime')
            .then(response => response.json())
            .then(data => setAllTimeExperts(data))
            .catch(error => console.error('Error fetching all-time experts:', error));

        fetch('http://localhost:3001/api/expert/top/today')
            .then(response => response.json())
            .then(data => setTodayExperts(data))
            .catch(error => console.error('Error fetching today\'s experts:', error));
    }, []);

    return (
        <div className="leaderboard">
            <h2 className='header-text'>Top 10 Experts of All Time</h2>
            <ol >
                {allTimeExperts.map((expert, index) => (
                    <li className='allTime' key={index}>
                        {expert.name} (<a href={expert.link} target="_blank">🕹️</a>) - Points: {expert.points}
                    </li>
                ))}
            </ol>
            <h2 className='header-text'>Top 10 Experts Today</h2>
            <ol>
                {todayExperts.map((expert, index) => (
                    <li className='today' key={index}>
                        {expert.name} - Points: {expert.points}
                    </li>
                ))}
            </ol>
        </div>
    );
}

export default Leaderboard;
