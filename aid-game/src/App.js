import './App.css';
import React, { useEffect, useState } from 'react';
import FeedbackForm from './components/FeedbackForm';
import Modal from './components/Modal';
import { BrowserRouter as Router, Route, Routes } from 'react-router-dom';
import CaseDisplay from './components/CaseDisplay';
import LoginPage from './components/LoginPage';
import AdminPanel from './components/AdminPanel';
import Leaderboard from './components/Leaderboard';
import MemoryGame from './components/MemoryGame';
import QuizGame from './components/QuizGame';
import InfoText from './components/InfoText';
import BackgroundMusic from './components/BackgroundMusic';

function App() {
  const [playVideo, setPlayVideo] = useState(false);
  const [buttonText, setButtonText] = useState('/');
  const [case_id, setImages] = useState([]);
  const [showLeaderboardVisible, setLeaderboardVisible] = useState(false);
  const [showInfoText, setInfoText] = useState(false);

  const [cards, setCards] = useState([]);
  const [questions, setQuestions] = useState([]);
  const [currentIndex, setCurrentIndex] = useState(0);
  const [showMemoryGame, setShowMemoryGame] = useState(false);
  const [showQuizGame, setShowQuizGame] = useState(false);
  const [currentPage, setCurrentPage] = useState('login');
  const [username, setUsername] = useState('');

  const [averageAccuracy, setAverageAccuracy] = useState(null);
  const [averageClarity, setAverageClarity] = useState(null);
  const [averageConsistency, setAverageConsistency] = useState(null);
  const [averageUsefulness, setAverageUsefulness] = useState(null);
  const [averageConfidence, setAverageConfidence] = useState(null);
  const [averageReliability, setAverageReliability] = useState(null);

  const [userAccuracy, setUserAccuracy] = useState(null);
  const [userClarity, setUserClarity] = useState(null);
  const [userConsistency, setUserConsistency] = useState(null);
  const [userUsefulness, setUserUsefulness] = useState(null);
  const [userConfidence, setUserConfidence] = useState(null);
  const [userReliability, setUserReliability] = useState(null);

  const [showFeedbackModal, setShowFeedbackModal] = useState(false);
  const [submitCount, setSubmitCount] = useState(0);
  const [userPoints, setUserPoints] = useState(0);

  useEffect(() => {
    fetch('http://localhost:3001/api/case')
      .then(response => response.json())
      .then(data => setImages(data))
      .catch(error => console.error('Error fetching images:', error));

    fetch('http://localhost:3001/api/cards')
      .then(response => response.json())
      .then(data => setCards(data))
      .catch(error => console.error('Error getting cards:', error));

    fetch('http://localhost:3001/api/quiz')
      .then(response => response.json())
      .then(data => {
        setQuestions(data);
      })
      .catch(error => console.error('Error fetching quiz questions:', error));

    fetchExpertData();

    if (submitCount > 0 && submitCount % 5 == 0) {
      toggleMemoryGame();
    }

    if (submitCount > 0 && submitCount % 3 == 0) {
      toggleQuizGame();
    }
  }, [submitCount, username]);

  const toggleLeaderboard = () => {
    setLeaderboardVisible((prevVisible) => !prevVisible);
  };

  const toggleInfoText = () => {
    setInfoText((prevVisible) => !prevVisible);
  };

  const toggleVideoPlayback = () => {
    setPlayVideo(!playVideo);

    if (playVideo) {
      setButtonText('/');
    } else {
      setButtonText(' ');
    }
  };

  const fetchExpertData = () => {
    if (username) {
      fetch(`http://localhost:3001/api/expert/${username}`)
        .then(response => response.json())
        .then(expert => {
          setUserPoints(expert.points);
        })
        .catch(error => console.error('Error fetching expert data:', error));
    }
  };

  const handleNextClick = () => {
    setCurrentIndex(prevIndex => (prevIndex + 1) % case_id.length);
  };

  const submitFeedbackForCase = (caseId, userRatings) => {
    setSubmitCount(prevCount => prevCount + 1);

    setUserAccuracy(userRatings.accuracy);
    setUserClarity(userRatings.clarity);
    setUserConsistency(userRatings.consistency);
    setUserUsefulness(userRatings.usefulness);
    setUserConfidence(userRatings.confidence);
    setUserReliability(userRatings.reliability);

    const theCase = case_id.find(c => c.case_id === caseId);
    if (theCase) {
      const ratings = [
        theCase.accuracy,
        theCase.clarity,
        theCase.consistency,
        theCase.usefulness,
        theCase.confidence,
        theCase.reliability
      ];
      handleSubmitFeedback(caseId, username, ratings);
      fetchExpertData();
      handleNextClick();
    } else {
      console.error("No case found with id:", caseId);
    }
  }

  const handleSubmitFeedback = (imageIdentity, username, ratings) => {
    if (!ratings || !Array.isArray(ratings) || ratings.length === 0) {
      console.error('Invalid or no ratings provided');
      return;
    }

    setAverageAccuracy(calculateAverage(ratings[0]));
    setAverageClarity(calculateAverage(ratings[1]));
    setAverageConsistency(calculateAverage(ratings[2]));
    setAverageUsefulness(calculateAverage(ratings[3]));
    setAverageConfidence(calculateAverage(ratings[4]));
    setAverageReliability(calculateAverage(ratings[5]));

    setShowFeedbackModal(true);
    console.log({ username }, "submitted feedback for case ID:", imageIdentity);
  };

  const calculateAverage = (arr) => arr.length > 0 ? arr.reduce((a, b) => a + b, 0) / arr.length : null;

  const handleStartClick = (enteredUsername) => {
    if (enteredUsername.trim() !== '') {
      setCurrentPage('main');
      setUsername(enteredUsername);
    } else {
      alert('Please enter your username.');
    }
  };

  const handleGameEnd = (finalScore) => {
    const newPoints = userPoints + finalScore;

    fetch('http://localhost:3001/api/expert/' + username, {
      method: 'PUT',
      headers: {
        'Content-Type': 'application/json',
      },
      body: JSON.stringify({ points: newPoints }),
    })
      .then(response => response.json())
      .then(data => {
        console.log("Points updated:", data);
        fetchExpertData(); // Re-fetch or update the local state to reflect the new points
      })
      .catch(error => console.error('Error:', error));
  };

  const toggleMemoryGame = () => {
    setShowMemoryGame(!showMemoryGame);
  };

  const toggleQuizGame = () => {
    setShowQuizGame(!showQuizGame);
  };

  const splash = document.querySelector('.loadIn');

  document.addEventListener('readystatechange', (e) => {
    setTimeout(() => {
      if (splash) {
        splash.classList.add('startEnde');
      } else {
        console.error('LoadIn element not found. Ansich funktioniert ja der Code nicht, aber es passiert das was soll :D. NEVER CHANGE A partly FUNCTIONING CODE');
      };
    }, 0);
  });

  return (
    <Router>
      <div class='loadIn'>
        <h1 class='fade-in'>TUMOR TAKEDOWN</h1>
      </div>
      <div className="App">
        <Routes>
          <Route path="/adminpanel" element={<AdminPanel />} />
          <Route path="/" element={currentPage === 'login' ?
            <LoginPage onStartClick={handleStartClick} /> :
            <div>
              <div>
                <h1 className='game-name'>TUMOR TAKEDOWN</h1>
                <button className='music' onClick={toggleVideoPlayback}> ♪ </button>
                <p className='stop-music' onClick={toggleVideoPlayback}>{buttonText}</p>
                <p className='game-info' onClick={toggleInfoText}>i</p>
                <Modal show={showInfoText} onClose={toggleInfoText}>
                  <InfoText />
                </Modal>
              </div>
              <div className='header-container'>
                <Modal show={showMemoryGame} onClose={toggleMemoryGame}>
                  <MemoryGame cards={cards} onFinish={handleGameEnd} />
                </Modal>
                <Modal show={showQuizGame} onClose={toggleQuizGame}>
                  <QuizGame questions={questions} onFinish={handleGameEnd} />
                </Modal>
                <button className='header-container-button' onClick={toggleLeaderboard}>LEADERBOARD</button>
                <Modal show={showLeaderboardVisible} onClose={toggleLeaderboard}>
                  <Leaderboard />
                </Modal>
              </div>
              <div className="username-display">
                Welcome, {username}!
              </div>
              <div className="level-points">
                Points: {userPoints} / Level: {case_id[currentIndex].case_id}
              </div>
              <div className='info-text'>
                The AI has presented a prognosis along with its reasoning for estimating the patient's survival time. <br></br> <br></br>
                Now, assess its explanation using different criteria and prove to your colleagues that you're the real expert.
              </div>
              <div className='prediction-text'>
                {case_id[currentIndex].explanation_text}
              </div>
              <div className="images-container">
                {case_id.length > 0 && <CaseDisplay case_id={case_id[currentIndex]} />}
                <FeedbackForm
                  imageIdentity={case_id[currentIndex].case_id}
                  onSubmitFeedback={submitFeedbackForCase}
                  username={username} />
                <Modal show={showFeedbackModal} onClose={() => setShowFeedbackModal(false)}>
                  <h2 className='textFB-head'> Good Job! Keep it going!</h2>
                  <h2 className='textFB-head'> Feedback Summary</h2>
                  <p className='textFB-title'> Accuracy: </p>
                  <p className='textFB'> Your Rating: {userAccuracy} / Average Rating: {averageAccuracy !== null ? averageAccuracy.toFixed(2) : "No data"}</p>
                  <p className='textFB-title'> Clarity: </p>
                  <p className='textFB'> Your Rating: {userClarity} / Average Rating: {averageClarity !== null ? averageClarity.toFixed(2) : "No data"}</p>
                  <p className='textFB-title'> Consistency: </p>
                  <p className='textFB'> Your Rating: {userConsistency} / Average Rating: {averageConsistency !== null ? averageConsistency.toFixed(2) : "No data"}</p>
                  <p className='textFB-title'> Usefulness: </p>
                  <p className='textFB'> Your Rating: {userUsefulness}/ Average Rating: {averageUsefulness !== null ? averageUsefulness.toFixed(2) : "No data"}</p>
                  <p className='textFB-title'> Confidence: </p>
                  <p className='textFB'> Your Rating: {userConfidence} / Average Rating: {averageConfidence !== null ? averageConfidence.toFixed(2) : "No data"}</p>
                  <p className='textFB-title'> Reliability:</p>
                  <p className='textFB'> Your Rating: {userReliability} / Average Rating: {averageReliability !== null ? averageReliability.toFixed(2) : "No data"}</p>
                </Modal>
              </div>
              <BackgroundMusic play={playVideo} />
            </div>
          } />
        </Routes>
      </div>
    </Router>
  );
}

export default App;