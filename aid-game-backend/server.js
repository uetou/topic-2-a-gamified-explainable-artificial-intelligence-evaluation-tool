const express = require('express');
const bodyParser = require('body-parser');
const cors = require('cors');

const app = express();
const port = process.env.PORT || 3001;

app.use(cors());
app.use(bodyParser.json());

app.get('/', (req, res) => {
    res.send('Hello from the backend!');
});

app.listen(port, () => {
    console.log(`Server running on port ${port}`);
});

const mongoose = require('mongoose');

mongoose.connect('mongodb://127.0.0.1:27017/aid-game-database', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.error('Could not connect to MongoDB...', err));

const Case = require('./models/CaseID');
const Card = require('./models/Card');
const Question = require('./models/Question');
const Expert = require('./models/Expert');

app.post('/api/feedback', async (req, res) => {
    const {
        caseId,
        accuracy,
        clarity,
        consistency,
        usefulness,
        confidence,
        reliability,
        additionalRemarks,
        username
    } = req.body;

    let pointsForFeedback = 0;

    let feedbackUpdates = {};

    if (accuracy) {
        pointsForFeedback += 5;
        feedbackUpdates.accuracy = accuracy;
    }
    if (clarity) {
        pointsForFeedback += 5;
        feedbackUpdates.clarity = clarity;
    }
    if (consistency) {
        pointsForFeedback += 5;
        feedbackUpdates.consistency = consistency;
    }
    if (usefulness) {
        pointsForFeedback += 5;
        feedbackUpdates.usefulness = usefulness;
    }
    if (confidence) {
        pointsForFeedback += 5;
        feedbackUpdates.confidence = confidence;
    }
    if (reliability) {
        pointsForFeedback += 5;
        feedbackUpdates.reliability = reliability;
    }
    if (additionalRemarks) {
        pointsForFeedback += 10;
        feedbackUpdates.additionalRemarks = additionalRemarks;
    }

    try {
        const updatedCase = await Case.findOneAndUpdate(
            { case_id: caseId },
            { $push: feedbackUpdates },
            { new: true }
        );

        const updatedExpert = await Expert.findOneAndUpdate(
            { name: username },
            { $inc: { level: 1, points: pointsForFeedback } },
            { new: true }
        );

        res.json({ updatedCase, updatedExpert });
    } catch (error) {
        res.status(500).send("Error updating case with feedback");
    }
});

app.get('/api/expert/top/alltime', async (req, res) => {
    try {
        const experts = await Expert.find({}).sort({ points: -1 }).limit(10);
        res.json(experts);
    } catch (error) {
        res.status(500).send("Error fetching experts: " + error.message);
    }
});

app.get('/api/expert/top/today', async (req, res) => {
    const today = new Date();
    today.setHours(0, 0, 0, 0);

    try {
        const experts = await Expert.find({ lastPointUpdate: { $gte: today } })
            .sort({ points: -1 })
            .limit(10);
        res.json(experts);
    } catch (error) {
        res.status(500).send("Error fetching experts: " + error.message);
    }
});

app.get('/api/expert', async (req, res) => {
    try {
        const experts = await Expert.find({});
        res.json(experts);
    } catch (error) {
        res.status(500).send("Error fetching expert data: " + error.message);
    }
});

app.post('/api/expert', async (req, res) => {
    const { name, link } = req.body;

    try {
        let expert = await Expert.findOne({ name });
        if (!expert) {
            expert = new Expert({ name, link, points: 0, level: 1 });
            await expert.save();
        }
        res.status(201).json(expert);
    } catch (error) {
        res.status(500).send("Error saving expert information");
    }
});

app.put('/api/expert/:name', async (req, res) => {
    try {
        const { points } = req.body; 
        const expert = await Expert.findOneAndUpdate({ name: req.params.name }, { $set: { points: points } }, { new: true });
        res.json(expert);
    } catch (error) {
        res.status(500).send("Error updating expert points");
    }
});

app.get('/api/expert/:name', async (req, res) => {
    try {
        const expert = await Expert.findOne({ name: req.params.name });
        if (!expert) {
            return res.status(404).send("Expert not found");
        }
        res.json(expert);
    } catch (error) {
        res.status(500).send("Error fetching expert data");
    }
});

app.get('/api/case', async (req, res) => {
    try {
        const cases = await Case.find({});
        res.json(cases);
    } catch (error) {
        res.status(500).send("Error fetching images");
    }
});

app.get('/api/cards', async (req, res) => {
    try {
        const cards = await Card.find({}); 
        res.json(cards);
    } catch (error) {
        res.status(500).send("Error fetching cards");
    }
});

app.get('/api/quiz', async (req, res) => {
    try {
        const quiz = await Question.find({});
        res.json(quiz);
    } catch (error) {
        res.status(500).send("Error fetching quiz questions");
    }
});

