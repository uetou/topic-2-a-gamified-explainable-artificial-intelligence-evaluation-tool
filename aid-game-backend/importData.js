const mongoose = require('mongoose');
const fs = require('fs');

const Case = require('./models/CaseID');
const Card = require('./models/Card');
const Question = require('./models/Question');
const Expert = require('./models/Expert');

mongoose.connect('mongodb://127.0.0.1:27017/aid-game-database', { useNewUrlParser: true, useUnifiedTopology: true })
    .then(() => console.log('Connected to MongoDB...'))
    .catch(err => console.error('Could not connect to MongoDB...', err));

const importData = async () => {
    try {
        await Case.deleteMany({});
        await Card.deleteMany({});
        await Question.deleteMany({});
        await Expert.deleteMany({});

        const data = JSON.parse(fs.readFileSync('data.json', 'utf-8'));

        await Case.insertMany(data.cases);
        await Card.insertMany(data.cards);
        await Question.insertMany(data.quiz);
        await Expert.insertMany(data.experts);

        console.log('Data successfully imported!');
    } catch (error) {
        console.error('Error importing data: ', error);
    } finally {
        mongoose.connection.close();
    }
};

importData();
