const mongoose = require('mongoose');

const cardSchema = new mongoose.Schema({
    url: { 
        type: String, 
        required: true 
    },
    name: { 
        type: String, 
        required: true 
    },
});

const Card = mongoose.model('Card', cardSchema);

module.exports = Card;
