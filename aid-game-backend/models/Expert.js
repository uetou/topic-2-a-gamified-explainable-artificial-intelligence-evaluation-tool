const mongoose = require('mongoose');

const ExpertSchema = new mongoose.Schema({
    name: String,
	link: String, 
	points: Number, 
	level: Number, 
	lastPointUpdate: { 
		type: Date, 
		default: Date.now 
	}
});

const expert = mongoose.model('Expert', ExpertSchema);
module.exports = expert;