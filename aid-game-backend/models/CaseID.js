const mongoose = require('mongoose');

const CaseSchema = new mongoose.Schema({
    source_image: String,
    heatmap_image: String,
    explanation_text: String,
    case_id: Number,
    accuracy: [Number],
    clarity: [Number],
    consistency: [Number],
    usefulness: [Number],
    confidence: [Number],
    reliability: [Number],
    additionalRemarks: [String]
});

const Case = mongoose.model('Case', CaseSchema);
module.exports = Case;
